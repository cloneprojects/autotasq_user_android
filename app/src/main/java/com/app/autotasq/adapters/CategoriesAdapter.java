package com.app.autotasq.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.autotasq.R;
import com.app.autotasq.activities.SubCategoriesActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 06/10/17.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder>{
    private Context context;
    private JSONArray categories;

    public CategoriesAdapter(Context context, JSONArray categories){
        this.context =context;
        this.categories = categories;
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryImage;
        LinearLayout categoryLayout;
        MyViewHolder(View view) {
            super(view);
            categoryName =  view.findViewById(R.id.categoryName);
            categoryImage = view.findViewById(R.id.categoryImage);
            categoryLayout = view.findViewById(R.id.categoryLayout);
        }
    }

    @Override
    public CategoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        return new CategoriesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesAdapter.MyViewHolder holder, int position) {
        try {
            final JSONObject subCategory = categories.getJSONObject(position);
            Log.d("onBindViewHolder: ","jsonObject"+subCategory);
            holder.categoryName.setText(subCategory.optString("category_name"));

            Glide.with(context).load(subCategory.optString("icon")).into(holder.categoryImage);
//            Glide.with(context).load("http://bmseelectrical.com/assets/uploads/image_uploads/ElectricalInstallation.jpg").into(holder.categoryImage);
            holder.categoryLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent timeAndAddress = new Intent(context, SubCategoriesActivity.class);
                    timeAndAddress.putExtra("subcategoriesArray",categories.toString());
                    timeAndAddress.putExtra("subCategoryId",subCategory.optString("id"));
                    context.startActivity(timeAndAddress);

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return categories.length();
    }
}


