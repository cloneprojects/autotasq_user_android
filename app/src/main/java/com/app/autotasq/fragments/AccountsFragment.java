package com.app.autotasq.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.app.autotasq.R;
import com.app.autotasq.activities.AboutUs;
import com.app.autotasq.activities.AddressActivity;
import com.app.autotasq.activities.AppSettings;
import com.app.autotasq.activities.ChangePasswordActivity;
import com.app.autotasq.activities.EditProfileActivity;
import com.app.autotasq.activities.FaqActivity;
import com.app.autotasq.activities.MainActivity;
import com.app.autotasq.activities.SignInActivity;
import com.app.autotasq.activities.TermsAndConditions;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountsFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static TextView userName, userMobile;
    public static ImageView profilePic;
    LinearLayout logOut;
    AppSettings appSettings;
    TextView logOutText;
    LinearLayout changeProfile;
    LinearLayout viewEditAddress;
    LinearLayout changePassword, guestLogin;
    // TODO: Rename and change types of parameters
    private String mParam1;
    GoogleApiClient mGoogleApiClient;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    public AccountsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountsFragment newInstance(String param1, String param2) {
        AccountsFragment fragment = new AccountsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_accounts, container, false);
        //noinspection deprecation
        FacebookSdk.sdkInitialize(getApplicationContext());
        initViews(view);

        setData();
        return view;
    }

    private void setData() {
        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            userName.setText("Hello!");
            userMobile.setText("Guest");
        } else {
            userName.setText(MainActivity.userDetails.optString("first_name") + " " + MainActivity.userDetails.optString("last_name"));
            userMobile.setText(MainActivity.userDetails.optString("mobile"));
            Glide.with(getActivity()).load(appSettings.getUserImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.profile_pic)).dontAnimate().into(profilePic);
        }

    }


    private void initViews(View view) {
        appSettings = new AppSettings(getActivity());

        userName = (TextView) view.findViewById(R.id.userName);
        changeProfile = (LinearLayout) view.findViewById(R.id.changeProfile);
        logOutText = (TextView) view.findViewById(R.id.logOutText);
        userMobile = (TextView) view.findViewById(R.id.userMobile);
        logOut = (LinearLayout) view.findViewById(R.id.logOut);
        profilePic = (ImageView) view.findViewById(R.id.profilePic);
        guestLogin = (LinearLayout) view.findViewById(R.id.guestLogin);
        viewEditAddress = (LinearLayout) view.findViewById(R.id.viewEditAddress);
        changePassword = (LinearLayout) view.findViewById(R.id.changePassword);
        View changePasswordView = (View) view.findViewById(R.id.changePasswordView);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });


        if (appSettings.getIsSocialLogin().equalsIgnoreCase("true")) {
            changePassword.setVisibility(View.GONE);
            changePasswordView.setVisibility(View.GONE);

        } else {
            changePassword.setVisibility(View.VISIBLE);
            changePasswordView.setVisibility(View.VISIBLE);
        }

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                moveSignInActivity();
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (mGoogleApiClient.isConnected()) {
                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                        mGoogleApiClient.disconnect();
                        mGoogleApiClient.connect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            guestLogin.setVisibility(View.GONE);
            logOutText.setText(getResources().getString(R.string.sign_in));
        } else {
            guestLogin.setVisibility(View.VISIBLE);
            logOutText.setText(getResources().getString(R.string.logout));

        }
        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("provider_details", "" + MainActivity.userDetails);
                startActivity(intent);
            }
        });
        initGeneral(view);
        viewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddressActivity.class);
                startActivity(intent);
            }
        });
        initGoogle();
    }

    private void initGoogle() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

    }

    private void initGeneral(View view) {

        final LinearLayout faq, aboutUs, termsAndCondition;
        faq = view.findViewById(R.id.faq);
        aboutUs = view.findViewById(R.id.aboutUs);
        termsAndCondition = view.findViewById(R.id.termsAndCondition);


        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqActivity.class);
                startActivity(intent);
            }
        });

        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AboutUs.class);
                startActivity(intent);
            }
        });

        termsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TermsAndConditions.class);
                startActivity(intent);
            }
        });
    }

    private void moveSignInActivity() {
        appSettings.setIsLogged("false");
        Intent intent = new Intent(getActivity(), SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
